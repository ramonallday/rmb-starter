# generator-rmb-starter [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> 

## Installation

First, install [Yeoman](http://yeoman.io) and generator-rmb-starter using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
```

As generator is not published link is needed:
```bash
npm link
```

Then generate your new project:

```bash
yo rmb-starter
```

## Getting To Know Yeoman

Yeoman has a heart of gold. He&#39;s a person with feelings and opinions, but he&#39;s very easy to work with. If you think he&#39;s too opinionated, he can be easily convinced. Feel free to [learn more about him](http://yeoman.io/).

## License

Apache-2.0 © [RAIMOND V]()


[npm-image]: https://badge.fury.io/js/generator-rmb-starter.svg
[npm-url]: https://npmjs.org/package/generator-rmb-starter
[travis-image]: https://travis-ci.org/ramonallday/generator-rmb-starter.svg?branch=master
[travis-url]: https://travis-ci.org/ramonallday/generator-rmb-starter
[daviddm-image]: https://david-dm.org/ramonallday/generator-rmb-starter.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/ramonallday/generator-rmb-starter

'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');

module.exports = yeoman.generators.Base.extend({
  prompting: function () {
    var done = this.async();

    this.log(yosay(
      'Welcome to the wonderful ' + chalk.red('RMB') + ' generator!'
    ));

    var prompts = [
          {
            type: 'input',
            name: 'name',
            message: 'Your banner name',
            //Defaults to the project's folder name if the input is skipped
            default: this.appname
          },
          {
            type: 'input',
            name: 'desc',
            message: 'Your banner description',
            default: 'dummy'
          },
          {
            type: 'input',
            name: 'width',
            message: 'Your banner width',
            default: '1'
          },

          {
            type: 'input',
            name: 'height',
            message: 'Your banner height',
            default: '1'
          }
    ];

    this.prompt(prompts, function (props) {
        this.props = props;

        this.log(yosay(
          'Badum tss ' + chalk.red(props.name) + ' ' + chalk.magenta(props.width) + 'x' + chalk.magenta(props.height)
        ));

        done();
    }.bind(this));


  },

  writing: function () {
        this.fs.copyTpl(
              this.templatePath('_package.json'),
              this.destinationPath('package.json'), {
                  name: this.props.name.replace(/\s+/g, '')
              }
        );

        this.fs.copy(
          this.templatePath('_gulpfile.js'),
          this.destinationPath('gulpfile.js'));

        this.fs.copyTpl(
              this.templatePath('_banner_manifest.json'),
              this.destinationPath('src/manifest.json'), {
                  name: this.props.name,
                  desc: this.props.desc,
                  width: this.props.width,
                  height: this.props.height
              }
        );

        this.fs.copyTpl(
              this.templatePath('_index.html'),
              this.destinationPath('src/index.html'), {
                  name: this.props.name
              }
        );

        this.fs.copy(
          this.templatePath('_main.js'),
          this.destinationPath('src/assets/scripts/main.js'));

        this.fs.copy(
          this.templatePath('_main.less'),
          this.destinationPath('src/assets/styles/main.less'));

  },

  install: function () {
    //link global to local node_modules
    this.spawnCommand('npm', ['start']);
  }
});

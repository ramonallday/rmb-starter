var fs = require('fs');
var gulp = require('gulp');
var inlinesource = require('gulp-inline-source');
var less         = require('gulp-less');
var jshint       = require('gulp-jshint');
var runSequence  = require('run-sequence');
// var gulpif       = require('gulp-if');
// var imagemin     = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var LessPluginAutoPrefix = require('less-plugin-autoprefix');
var autoprefixer = new LessPluginAutoPrefix({ browsers: [
        'last 2 versions',
        'ie 8',
        'ie 9',
        'android 2.3',
        'android 4',
        'opera 12'
      ] });
 
var uglifyOptions = {
  mangle: false,
  output: {
    indent_start  : 0,     // start indentation on every line (only when `beautify`)
    indent_level  : 4, 
    beautify: true
  },
  compress: false,
};

var path = {
  "index": "src/index.html",
  "manifest": "src/manifest.json",
  "src" : "src/",
  "source": "src/assets/",
  "compiled": "src/compiled_sources",
  "dist": "./dist/"
};

gulp.task('less', function() {

  return gulp.src(path.source + 'styles/main.less')
    .pipe(less({
      plugins: [autoprefixer]
    }))
    .pipe(gulp.dest(path.compiled));

});


gulp.task('scripts', function() {
  return gulp.src(path.source + 'scripts/main.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    //uglify
    .pipe(uglify(uglifyOptions))
    .pipe(gulp.dest(path.compiled));
});

// ### Images
// `gulp images` - Run lossless compression on all the images.
// gulp.task('images', function() {
//   return gulp.src(globs.images)
//     .pipe(imagemin({
//       progressive: true,
//       interlaced: true,
//       svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
//     }))
//     .pipe(gulp.dest(path.dist + 'images'));
//     //.pipe(browserSync.stream());
// });


gulp.task('manifest', function() {

  return gulp.src(path.manifest)
        .pipe(gulp.dest('./dist'));

});

gulp.task('html', function() {

  return gulp.src(path.index)
        .pipe(inlinesource({ compress: false }))
        .pipe(gulp.dest('./dist'));

});


gulp.task('fiddle', function() {

  fs.exists(path.dist + 'index.html', function(exists) {
    if (exists) {
      fs.readFile(path.dist + 'index.html', {encoding: 'utf-8'}, function(err, file) {
        var script = 'document.write(' + JSON.stringify(file) + ');';

          fs.writeFile('fiddle.js', script, function (err) {
            if (err) throw err;
            console.log('Fiddle file updated!');
          });

      });
    }
  });
});


gulp.task('default', function() {
  runSequence(
    ['manifest'],
    ['scripts'],
    ['less'],
    ['html'],
    ['fiddle']
  );
});


gulp.task('watch', function() {

  gulp.watch([path.index], ['default']);
  gulp.watch([path.source + 'styles/main.less'], ['default']);
  gulp.watch([path.source + 'scripts/**/*'], ['default']);
  //gulp.watch([path.source + 'fonts/**/*'], ['fonts']);
  gulp.watch([path.source + 'images/**/*'], ['images', 'manifest']);
});